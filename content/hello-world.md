Title: Hello World!
Date: 2023-03-26 21:05
Category: MeMyselfAndI

Hello World!

Welcome to iWannaBe.dev! My name is Marcin, and I'm a developer wannabe with a passion for Python programming. I'm also a tech geek with a wide range of interests, including DIY projects, electronics, home automation, and more.

I created this blog to share my experiences and insights as I learn more about the world of programming and technology. Whether you're an experienced developer or just starting out, I hope you'll find something here that inspires you.

In the coming weeks and months, I'll be sharing tutorials, tips and tricks, and other resources to help you become a better programmer. I'll also be sharing my own projects and experiments, so you can see what I'm working on and maybe even learn something new.

Thanks for stopping by, and I hope you'll come back soon!